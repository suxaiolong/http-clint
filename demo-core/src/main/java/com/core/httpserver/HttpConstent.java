package com.core.httpserver;

import org.omg.CORBA.INTERNAL;

/**
 * http常量池
 */
public class HttpConstent {
    //控制类包路径
    public static String HANDLER_PACKAGE = "com.node.controller";
    //Io 线程数量
    public static Integer IO_THREAD_COUNT = 40;
    //读取数据阻塞链路的超时时间 毫秒
    public static Integer SO_TIME_OUT = 3000;
    //关闭Socket后,还可以立即重用端口
    public static boolean SO_REUSE_ADDRESS = true;
    // 设置socket延迟时间,当关闭时,会进入阻塞状态,直到全部数据都发送完或者超时
    public static Integer SO_LINGER = -1;
    //检测是否存活
    public static boolean SO_KEEP_ALIVE = false;
    //true：允许小包的发送,数据传输量比较小的应用,
    //false：等缓冲区达到一个量级才会发送,提高网络利用率,但会有网络延迟
    public static boolean TCP_NO_DELAY = true;
    //链接超时时间 毫秒单位
    public static Integer CONNECT_TIME_OUT = 3000;
    //发送缓冲区大小
    public static Integer SND_BUF_SIZE = 2048;
    //接受缓冲区大小
    public static Integer RCV_BUF_SIZE = 1024;
    //日志大小
    public static Integer BACK_LOG_SIZE = 128;
    //端口
    public static Integer PORT = 8991;
    //服务信息
    public static String SERVER_INFO = "demo/1.0";
}
