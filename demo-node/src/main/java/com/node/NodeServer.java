package com.node;

import com.core.httpserver.HttpServerBootstrap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeServer {
    private static final Logger logger = LoggerFactory.getLogger(NodeServer.class);

    public static void main(String[] args) {
        try {
            HttpServerBootstrap.getInstance().start();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("http start error={}", e);
        }
        logger.info("初始化 http start  end ");
    }

}
